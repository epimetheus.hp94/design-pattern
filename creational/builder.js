/**
 * The design pattern we will be going over is commonly known as the Builder design pattern,
 * a pattern used to help construct complex objects. It helps separate object construction from its representation,
 * which will help us reuse this to create different representations.
 */

class Frog {
  constructor(name, weight, height, gender) {
    this.name = name;
    this.weight = weight;
    this.height = height;
    this.gender = gender;
  }

  eat(target) {
    console.log(`Eating target: ${target.nameß}`)
  }

  sayHi() {
    console.log(this.name);
  }
}

class FrogBuilder {
  constructor(name, gender) {
    this.name = name;
    this.gender = gender;
  }

  setWeight(weight) {
    this.weight = weight;
    return this;
  };

  setHeight(height) {
    this.height = height;
    return this;
  }

  build() {
    if (!('weight' in this)) {
      throw new Error('Weight is missing')
    }
    if (!('height' in this)) {
      throw new Error('Height is missing')
    }
    return new Frog(this.name, this.weight, this.height, this.gender)
  }
}

const frog = new FrogBuilder('Leon', 'male')
.setHeight(200)
.setWeight(100)
.build();

